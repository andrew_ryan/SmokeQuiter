//
//  SmokeQuiterApp.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/10.
//

import SwiftUI

@main
struct SmokeQuiterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
