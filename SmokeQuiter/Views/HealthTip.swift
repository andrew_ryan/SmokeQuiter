//
//  HealthTip.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/11.
//

import SwiftUI

struct HealthTip: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        GeometryReader { _ in
            VStack {
                main
            }.navigationTitle("健康提示")
        }
    }

    // MARK: header

    var header: some View {
        // header
        HStack {
            // back button
            Button(action: {
                dismiss()
            }, label: {
                Image(systemName: "chevron.left")
                    .font(.headline)
                    .bold()
                    .foregroundColor(colorScheme == .dark ? .white : .black)
                    .contentShape(Rectangle())
            })

            // title
            Text("健康提示")
                .font(.title2)
                .bold()
                .foregroundColor(colorScheme == .dark ? .white : .black)
                .hAlign(.center)

        }.padding()
    }

    // MARK: main

    var main: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ScrollViewReader { _ in
                ForEach(Array(HealthTipModel.enumerated()), id: \.offset) { _, item in
                    VStack {
                        Text(item.0)
                            .font(.headline)
                            .foregroundColor(colorScheme == .dark ? .white : .black)
                            .hAlign(.leading)

                        Text(item.1)
                            .font(.callout)
                            .foregroundColor(colorScheme == .dark ? .white : .black)
                            .hAlign(.leading)
                            .padding(.top, 5)
                    }.hAlign(.leading).padding(.horizontal, 20).padding(.vertical, 5)
                }
            }
        }
    }
}

#Preview {
    HealthTip()
}
