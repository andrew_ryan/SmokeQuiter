//
//  TodaySmokedCigarettes.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/11.
//

import SwiftUI

struct TodaySmokedCigarettes: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.colorScheme) var colorScheme
    @State var TodaySmokedCigarettesList: [[String]] = []
    @Binding var todaySmokedTobacoData: Data
    var body: some View {
        GeometryReader { _ in
            VStack {
                main.padding()
            }.navigationTitle("今日抽烟列表")
        }
        .onAppear {
            if let todaySmokedCigarettesModel: TodaySmokedCigarettesModel = todaySmokedTobacoData.decode() {
                let AllCigarettes: [[String]] = todaySmokedCigarettesModel.allCigarettes
                var NewTodaySmokedCigarettesList: [[String]] = []
                for cigarette in AllCigarettes {
                    let todayTime = Date().toString("yyyy:MM:dd:HH:mm:ss").prefix(10).description.suffix(5).description
                    let time = cigarette.last?.description.prefix(10).description.suffix(5).description ?? Date().toString("yyyy:MM:dd:HH:mm:ss").prefix(10).description.suffix(5).description
                    if time.contains(todayTime) {
                        NewTodaySmokedCigarettesList.append(cigarette)
                    }
                }
                TodaySmokedCigarettesList = NewTodaySmokedCigarettesList
            }
        }
    }

    // MARK: main

    var main: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ScrollViewReader { _ in
                ForEach(Array(TodaySmokedCigarettesList.enumerated()), id: \.offset) { index, item in
                    tobacoItemView(item: item, index: index)
                }
                if TodaySmokedCigarettesList.isEmpty {
                    Text("今天没有抽烟").hAlign(.center)
                }
            }
        }
    }

    // MARK: tobaco Item View

    @ViewBuilder
    func tobacoItemView(item: [String], index _: Int) -> some View {
        HStack {
            if item.count > 8 {
                WebImg(imgURL: item[1]).frame(width: 80)
                VStack {
                    Text(item[2]).padding(.bottom, 3).hAlign(.leading).font(.headline)
                    Text("单盒参考价格：\(item[3])").padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text("条盒参考价格：\(item[4])").padding(.bottom, 3).hAlign(.leading).font(.caption)
                }.vAlign(.top).padding(.leading, 15)
                VStack {
                    Text("").padding(.bottom, 3).hAlign(.leading).font(.headline)
                    Text(item[5]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text(item[6]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text(item[7]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                }.vAlign(.top)
                    .padding(.leading, 5)
            }
        }
        .hAlign(.leading)
        .padding(.horizontal, 10)
    }
}
