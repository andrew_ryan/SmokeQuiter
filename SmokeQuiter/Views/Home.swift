//
//  Home.swift
//  HealthHabitCompanion
//
//  Created by Ryan Andrew on 2023/6/23.
//

import SwiftUI

struct Home: View {
    @State private var isShowingSheet: Bool = false
    @Binding var leftModalWidth: CGFloat
    @Binding var showLeftModal: Bool
    @Binding var smokingTimeInterval: Date
    @Binding var userData: Data
    @Binding var todaySmokedTobacoData: Data
    @State var searchTobacoText: String = ""
    @State private var motivationalStatement: String = GetMotivationalStatement()
    @State private var tobacoListModel: [[String]] = Array(TobacoListModel().prefix(10)) // 所有烟的前10个
    @AppStorage("userSlectedTobaco") var userSlectedTobaco: String = ""
    @AppStorage("userSlectedTobacoImageURL") var userSlectedTobacoImageURL: String = ""
    @AppStorage("todayTobacoCount") var todayTobacoCount: String = "0"
    @AppStorage("todayTarCount") var todayTarCount: Double = 0.0
    @AppStorage("todayNicotineCount") var todayNicotineCount: Double = 0.0
    @AppStorage("todayCOCount") var todayCOCount: Double = 0.0
    @AppStorage("todaySmokeConsumption") var todaySmokeConsumption: String = "0.0"
    @AppStorage("lastSmokeTime") var lastSmokeTime: String = "刚刚"
    @AppStorage("lastSmokeTimeProgess") var lastSmokeTimeProgess: Double = 0.0
    @AppStorage("daysOfContinuousExecutionPlan") var daysOfContinuousExecutionPlan: Int = 0
    let timer = Timer.publish(every: 30, on: .main, in: .common).autoconnect()

    // MARK: body

    var body: some View {
        GeometryReader { geometryReader in
            ZStack {
                VStack {
                    header(geometryReader: geometryReader)
                    main
                    footer
                }
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            let offset = value.translation.width
                            if offset > 100 {
                                leftModalWidth = offset
                                showLeftModal = true
                            }
                        }
                        .onEnded { _ in
                            withAnimation(.linear) {
                                if leftModalWidth > 150 {
                                    leftModalWidth = geometryReader.size.width * (2 / 3)
                                    showLeftModal = true
                                } else {
                                    leftModalWidth = .zero
                                    showLeftModal = false
                                }
                            }
                        }
                )
            }
        }
    }

    // MARK: header

    @ViewBuilder
    func header(geometryReader: GeometryProxy) -> some View {
        HStack {
            Image(systemName: "text.justify")
                .font(.title2)
                .onTapGesture {
                    showLeftModal = true
                    leftModalWidth = geometryReader.size.width * (2 / 3)
                }
            Spacer()

            // MARK: share button

            Image(systemName: "square.and.arrow.up")
                .font(.title2)
                .onTapGesture {
                    // MARK: tap to share Image

                    var items: [Any] = []
                    if let IndexUIImage = Index().toImage() {
                        items.append(IndexUIImage)
                    }
//                    if let MilestoneUIImage = Milestone(userData: $userData).toImage(){
//                        items.append(MilestoneUIImage)
//                    }
                    if !items.isEmpty {
                        ShareController().shareToOtherApp(items)
                    }
                }
        }.padding(.horizontal, 20)
    }

    // MARK: sixCards

    var sixCards: some View {
        VStack {
            // MARK: top three card

            Text("\(userSlectedTobaco)").font(.footnote).hAlign(.leading).padding(.vertical, 5)
            HStack {
                card.overlay {
                    VStack {
                        Image("tobaco").resizable().foregroundColor(Color(hex: "F4E1E1")!).frame(width: 30, height: 30).hAlign(.leading).padding(.bottom, 5)
                        Text("今日抽烟量").font(.footnote).hAlign(.leading).padding([.top, .bottom], 3)
                        Text("\(todayTobacoCount)支").font(.footnote).fontWeight(.bold).hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }
                card.overlay {
                    VStack {
                        CircleProgress(progress: lastSmokeTimeProgess).frame(width: 25, height: 25)
                            .hAlign(.leading)
                        Text("上次抽烟").font(.footnote).hAlign(.leading).padding([.top, .bottom], 3)
                        Text(lastSmokeTime).font(.footnote).fontWeight(.bold).hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }

                card.overlay {
                    VStack {
                        Image(systemName: "dollarsign")
                            .bold()
                            .frame(width: 30, height: 30)
                            .foregroundColor(Color(hex: "F4E1E1")!)
                            .hAlign(.leading)
                            .padding(.bottom, 5)

                        Text("今日抽烟消耗")
                            .font(.caption)
                            .hAlign(.leading)
                            .padding([.top, .bottom], 3)

                        Text("RMB \(todaySmokeConsumption)")
                            .font(.footnote)
                            .fontWeight(.bold)
                            .hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }
            }.hAlign(.center)

            // MARK: bottom three card

            Text("今日的有害成分含量").font(.footnote).hAlign(.leading).padding(.vertical, 5)
            HStack {
                card.overlay {
                    VStack {
                        Image("tar").resizable().foregroundColor(Color(hex: "F4E1E1")!).frame(width: 30, height: 30).hAlign(.leading).padding(.bottom, 5)
                        Text("焦油量").font(.footnote).hAlign(.leading).padding([.top, .bottom], 3)
                        Text("\(String(format: "%.1f", todayTarCount)) mg").font(.footnote).fontWeight(.bold).hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }
                card.overlay {
                    VStack {
                        Image("nicotine").resizable().foregroundColor(Color(hex: "F4E1E1")!).frame(width: 30, height: 30).hAlign(.leading).padding(.bottom, 5)

                        Text("尼古丁量").font(.footnote).hAlign(.leading).padding([.top, .bottom], 3)
                        Text("\(String(format: "%.1f", todayNicotineCount)) mg")
                            .font(.footnote).fontWeight(.bold).hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }

                card.overlay {
                    VStack {
                        Image("co").resizable().foregroundColor(Color(hex: "F4E1E1")!).frame(width: 35, height: 35).hAlign(.leading).padding(.bottom, 5)

                        Text("一氧化碳量")
                            .font(.caption)
                            .hAlign(.leading)
                            .padding([.top, .bottom], 3)

                        Text("\(String(format: "%.1f", todayCOCount)) mg")
                            .font(.footnote)
                            .fontWeight(.bold)
                            .hAlign(.leading)
                    }.padding().vAlign(.leading).foregroundColor(.white)
                }
            }.hAlign(.center)
        }
    }

    // MARK: main

    var main: some View {
        ScrollView {
            VStack {
                Text("今日抽烟").font(.title).fontWeight(.bold).hAlign(.leading)
                sixCards.padding(.vertical, 5)
                Text(motivationalStatement).font(.body).hAlign(.center).padding(.vertical, 5)
                Button(
                    action: {
                        isShowingSheet.toggle()
                    },
                    label: { CircleBotton().foregroundColor(.white) }
                ).sheet(isPresented: $isShowingSheet) {
                    TobacoInfo(isShowingSheet: $isShowingSheet,
                               searchTobacoText: $searchTobacoText, userSlectedTobaco: $userSlectedTobaco, todayTobacoCount: $todayTobacoCount, todaySmokeConsumption: $todaySmokeConsumption, lastSmokeTime: $lastSmokeTime, lastSmokeTimeProgess: $lastSmokeTimeProgess, smokingTimeInterval: $smokingTimeInterval,
                               userData: $userData, tobacoListModel: $tobacoListModel, todaySmokedTobacoData: $todaySmokedTobacoData).padding()
                } // end Button
            }
            .padding(.all, 20)
            .hAlign(.leading)

            // MARK: onChange of isShowingSheet

            .onChange(of: isShowingSheet, perform: { value in
                if value == false {
                    // MARK: set motivationalStatement

                    motivationalStatement = GetMotivationalStatement()

                    if let AllUserData: allUserData = userData.decode() {
                        let AllUserItems = AllUserData.allUserItems

                        // MARK: set daysOfContinuousExecutionPlan

                        daysOfContinuousExecutionPlan = getDaysOfContinuousExecutionPlan(from: AllUserItems)

                        if AllUserItems.count >= 1 {
                            let lastItem = Array(AllUserItems.suffix(1)).first!
                            let tobacoListModelItem = tobacoListModel[lastItem.tobacoListIndex]

                            // MARK: set userSlectedTobaco

                            userSlectedTobaco = tobacoListModelItem[2]

                            // MARK: set userSlectedTobacoImageURL

                            userSlectedTobacoImageURL = tobacoListModelItem[1]

                            // MARK: set todayTobacoCount

                            todayTobacoCount = "\(getAllTodayUserData(from: AllUserItems).count)"

                            // MARK: set todayTarCount

                            var getTodayTarCount: Double {
                                var result = 0.0
                                for item in getAllTodayUserData(from: AllUserItems) {
                                    if tobacoListModel.count > item.tobacoListIndex {
                                        let tobacoPrice = Double(tobacoListModel[item
                                                .tobacoListIndex][6]
                                            .replace(from: "焦油量:", to: "")
                                            .replace(from: "mg", to: "")
                                        )
                                        result += tobacoPrice ?? 0.0
                                    }
                                }
                                return result
                            }
                            todayTarCount = getTodayTarCount

                            // MARK: set todayNicotineCount

                            var getTodayNicotineCount: Double {
                                var result = 0.0
                                for item in getAllTodayUserData(from: AllUserItems) {
                                    if tobacoListModel.count > item.tobacoListIndex {
                                        let tobacoPrice = Double(tobacoListModel[item
                                                .tobacoListIndex][7]
                                            .replace(from: "烟碱量:", to: "")
                                            .replace(from: "mg", to: "")
                                        )
                                        result += tobacoPrice ?? 0.0
                                    }
                                }
                                return result
                            }
                            todayNicotineCount = getTodayNicotineCount

                            // MARK: set todayCOCount

                            var getTodayCOCount: Double {
                                var result = 0.0
                                for item in getAllTodayUserData(from: AllUserItems) {
                                    if tobacoListModel.count > item.tobacoListIndex {
                                        let tobacoPrice = Double(tobacoListModel[item
                                                .tobacoListIndex][8]
                                            .replace(from: "一氧化碳量:", to: "")
                                            .replace(from: "mg", to: "")
                                        )
                                        result += tobacoPrice ?? 0.0
                                    }
                                }
                                return result
                            }
                            todayCOCount = getTodayCOCount

                            // MARK: set todaySmokeConsumption

                            var getTodaySmokeConsumption: String {
                                var result = 0.0
                                for item in getAllTodayUserData(from: AllUserItems) {
                                    if tobacoListModel.count > item.tobacoListIndex {
                                        let tobacoPrice = Double(tobacoListModel[item.tobacoListIndex][3])
                                        result += ((tobacoPrice ?? 0.0) / 10)
                                    }
                                }
                                return String(format: "%.1f", result)
                            }

                            todaySmokeConsumption = getTodaySmokeConsumption
                        }

                        // MARK: set lastSmokeTime

                        if AllUserItems.count >= 2 {
                            let lastTwoItems = Array(AllUserItems.suffix(1))
                            let gapInSeconds = Date().timeIntervalSince1970 - lastTwoItems[0].time.timeIntervalSince1970

                            // MARK: set lastSmokeTimeProgess

                            lastSmokeTimeProgess = gapInSeconds / Double(smokingTimeInterval.getSeconds)

                            let gapInMinutes = Int(gapInSeconds / 60)
                            let gapHours = Int(gapInMinutes / 60)
                            if gapHours == 0, gapInMinutes == 0 {
                                lastSmokeTime = "\(Int(gapInSeconds)) s"
                            } else if gapHours == 0 {
                                lastSmokeTime = "\(gapInMinutes) min"
                            } else {
                                lastSmokeTime = "\(gapHours) h \(gapInMinutes) min"
                            }
                        }
                    }
                }
            }) // end onChange

            // MARK: start onReceive

            .onReceive(timer) { _ in
                if let AllUserData: allUserData = userData.decode() {
                    let AllUserItems = AllUserData.allUserItems

                    // MARK: set lastSmokeTime

                    if AllUserItems.count >= 1 {
                        let lastTwoItems = Array(AllUserItems.suffix(1))
                        let gapInSeconds = Date().timeIntervalSince1970 - lastTwoItems[0].time.timeIntervalSince1970

                        // MARK: set lastSmokeTimeProgess

                        lastSmokeTimeProgess = gapInSeconds / Double(smokingTimeInterval.getSeconds)

                        let gapInMinutes = Int(gapInSeconds / 60)
                        let gapHours = Int(gapInMinutes / 60)
                        if gapHours == 0, gapInMinutes == 0 {
                            lastSmokeTime = "\(Int(gapInSeconds)) s"
                        } else if gapHours == 0 {
                            lastSmokeTime = "\(gapInMinutes) min"
                        } else {
                            lastSmokeTime = "\(gapHours) h \(gapInMinutes) min"
                        }
                    }
                }
            }

            // MARK: onChange smokingTimeInterval

            .onChange(of: smokingTimeInterval, perform: { _ in
                if let AllUserData: allUserData = userData.decode() {
                    let AllUserItems = AllUserData.allUserItems

                    // MARK: set lastSmokeTime

                    if AllUserItems.count >= 1 {
                        let lastTwoItems = Array(AllUserItems.suffix(1))
                        let gapInSeconds = Date().timeIntervalSince1970 - lastTwoItems[0].time.timeIntervalSince1970

                        // MARK: set lastSmokeTimeProgess

                        lastSmokeTimeProgess = gapInSeconds / Double(smokingTimeInterval.getSeconds)

                        let gapInMinutes = Int(gapInSeconds / 60)
                        let gapHours = Int(gapInMinutes / 60)
                        if gapHours == 0, gapInMinutes == 0 {
                            lastSmokeTime = "\(Int(gapInSeconds)) s"
                        } else if gapHours == 0 {
                            lastSmokeTime = "\(gapInMinutes) min"
                        } else {
                            lastSmokeTime = "\(gapHours) h \(gapInMinutes) min"
                        }
                    }
                }
            })
        }
    }

    // MARK: footer

    var footer: some View {
        HStack {}
    }

    // MARK: card

    var card: some View {
        Rectangle()
            .foregroundColor(.clear)
            .frame(width: 100, height: 100)
            .background(
                LinearGradient(
                    stops: [
                        Gradient.Stop(color: Color(red: 0.15, green: 0.51, blue: 0.71), location: 0.00),
                        Gradient.Stop(color: Color(red: 0.27, green: 0.18, blue: 0.52).opacity(0.96), location: 0.06),
                        Gradient.Stop(color: Color(red: 0.16, green: 0.21, blue: 0.66).opacity(0.77), location: 0.36),
                        Gradient.Stop(color: Color(red: 0.22, green: 0.13, blue: 0.8).opacity(0.54), location: 0.71),
                        Gradient.Stop(color: Color(red: 0.19, green: 0.1, blue: 0.38).opacity(0.46), location: 1.00),
                    ],
                    startPoint: UnitPoint(x: 0.14, y: 0),
                    endPoint: UnitPoint(x: 0.5, y: 1)
                )
            )
            .cornerRadius(10)
            .frame(width: 100, height: 100).padding(.horizontal, 5)
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Index()
    }
}
