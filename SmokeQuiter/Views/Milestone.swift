//
//  Milestone.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/9.
//

import Charts
import SwiftUI

struct Milestone: View, Hashable {
    static func == (lhs: Milestone, rhs: Milestone) -> Bool {
        lhs.userData == rhs.userData
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(userData)
    }

    @Binding var userData: Data
    @Environment(\.dismiss) private var dismiss
    @Environment(\.colorScheme) var colorScheme
    @AppStorage("showChartWeekOrDay") var showChartWeekOrDay: String = "一天"
    @State private var WeekOrDay = ["一天", "一周"]
    @State private var AllUserItems: [userDataItem] = []
    var body: some View {
        GeometryReader { geometryReader in
            VStack {
                Picker("WeekOrDay", selection: $showChartWeekOrDay) {
                    ForEach(WeekOrDay, id: \.self) { flavor in
                        Text(flavor)
                    }
                }.pickerStyle(.segmented)

                if showChartWeekOrDay == "一周" {
                    // MARK: Week charts

                    if getMileStoneWeekData(from: AllUserItems).count > 0 {
                        PointMarkChart(getMileStoneData: getMileStoneWeekData(from: AllUserItems)).padding(.horizontal, 15)
                    } else {
                        Text("今天还没有抽烟数据")
                    }
                } else {
                    // MARK: Today charts

                    if getMileStoneTodayData(from: AllUserItems).count > 0 {
                        PointMarkChart(getMileStoneData: getMileStoneTodayData(from: AllUserItems)).padding(.horizontal, 15)
                    } else {
                        Text("这周没有抽烟数据")
                    }
                }
                Spacer()
            }
            .navigationTitle("里程碑")
            .hAlign(.leading)
            .frame(height: geometryReader.size.height * 0.9)
            .padding()
            .onAppear {
                if let AllUserData: allUserData = userData.decode() {
                    AllUserItems = AllUserData.allUserItems
                }
            }
            .gesture(
                DragGesture()
                    .onChanged { _ in
                        //   print(value.translation.width)
                    }
                    .onEnded { _ in
                    }
            )
        }
    }
}

struct Milestone_Previews: PreviewProvider {
    static var previews: some View {
        Index()
    }
}
