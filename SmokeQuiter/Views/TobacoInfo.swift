//
//  TobacoInfo.swift
//  HealthHabitCompanion
//
//  Created by Ryan Andrew on 2023/6/24.
//

import SwiftUI

struct TobacoInfo: View {
    @Binding var isShowingSheet: Bool
    @Binding var searchTobacoText: String
    @Binding var userSlectedTobaco: String
    @Binding var todayTobacoCount: String
    @Binding var todaySmokeConsumption: String
    @Binding var lastSmokeTime: String
    @Binding var lastSmokeTimeProgess: Double
    @Binding var smokingTimeInterval: Date
    @Binding var userData: Data
    @Binding var tobacoListModel: [[String]]
    @Binding var todaySmokedTobacoData: Data
    @State private var showMore = true
    @State private var showNotFound = false
    @State private var stopIndex = 10

    var body: some View {
        GeometryReader { _ in
            VStack {
                HStack {
                    Button(action: {
                        isShowingSheet = false
                    }, label: {
                        Image(systemName: "xmark.circle")
                            .resizable()
                            .font(.headline)
                            .foregroundColor(.red)
                            .frame(width: 30, height: 30)
                    })

                    Spacer()
                }
                // 利群

                // MARK: search TextField

                TextField("", text: $searchTobacoText).addPrefixIcon(icon: "magnifyingglass").padding()
                    .onSubmit {
                        if !searchTobacoText.isEmpty {
                            let searchList: [[String]] = TobacoListModel().filter { tobaco in
                                tobaco[2].contains(searchTobacoText)
                            }.sorted { tobaco1, tobaco2 in
                                tobaco1[2] < tobaco2[2]
                            }
                            if searchList.isEmpty {
                                showNotFound = true
                            }
                            tobacoListModel = searchList

                        } else {
                            tobacoListModel = Array(TobacoListModel().prefix(10))
                        }
                    }
                ScrollView(.vertical, showsIndicators: false) {
                    ScrollViewReader { _ in
                        ForEach(Array(tobacoListModel.enumerated()), id: \.offset) { index, item in
                            tobacoItemView(item: item, index: index)
                        }
                        if showNotFound {
                            Text("没有找到类似的香烟")
                        }
                    }
                    if showMore {
                        Button("加载更多") {
                            loadData()
                        }
                        .frame(width: 200, height: 50)
                    }
                }
            }
            .onChange(of: searchTobacoText, perform: { value in
                if value == "" {
                    tobacoListModel = Array(TobacoListModel().prefix(10))
                } else {
                    showMore = false
                }
            })
            .padding(.horizontal, 15)
        }
    }

    // MARK: selectBotton

    @ViewBuilder
    func selectBotton() -> some View {
        Rectangle()
            .foregroundColor(.clear)
            .frame(width: 57, height: 30)
            .background(
                LinearGradient(
                    stops: [
                        Gradient.Stop(color: Color(red: 0.46, green: 0.84, blue: 0.63), location: 0.00),
                        Gradient.Stop(color: Color(red: 0.46, green: 0.23, blue: 0.85).opacity(0.34), location: 1.00),
                    ],
                    startPoint: UnitPoint(x: 0.5, y: 0),
                    endPoint: UnitPoint(x: 0.5, y: 1)
                )
            )
            .cornerRadius(5)
    }

    // MARK: tobaco Item View

    @ViewBuilder
    func tobacoItemView(item: [String], index: Int) -> some View {
        HStack {
            if item.count > 8 {
                WebImg(imgURL: item[1]).frame(width: 80)
                VStack {
                    Text(item[2]).padding(.bottom, 3).hAlign(.leading).font(.headline)
                    Text("单盒参考价格：\(item[3])").padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text("条盒参考价格：\(item[4])").padding(.bottom, 3).hAlign(.leading).font(.caption)
                }.vAlign(.top).padding(.leading, 15)
                VStack {
                    Text("").padding(.bottom, 3).hAlign(.leading).font(.headline)
                    Text(item[5]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text(item[6]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Text(item[7]).padding(.bottom, 3).hAlign(.leading).font(.caption)
                    Button(action: {
                        // MARK: update userData

                        let UserDataItem: userDataItem = .init(time: Date(), tobacoListIndex: index)
                        if let AllUserData: allUserData = userData.decode() {
                            var AllUserItems = AllUserData.allUserItems
                            AllUserItems.append(UserDataItem)
                            AllUserItems = removeIfTimeIsWeekAgo(from: AllUserItems)

                            let AllUserData = allUserData(allUserItems: AllUserItems)

                            if let newUserData = AllUserData.encode() {
                                userData = newUserData
                            }
                        } else {
                            let AllUserItems = [UserDataItem]
                            let AllUserData = allUserData(allUserItems: AllUserItems)
                            if let newUserData = AllUserData.encode() {
                                userData = newUserData
                            }
                        }
                        isShowingSheet = false

                        // MARK: update todaySmokedTobacoData

                        if let todaySmokedCigarettesModel: TodaySmokedCigarettesModel = todaySmokedTobacoData.decode() {
                            var AllCigarettes: [[String]] = todaySmokedCigarettesModel.allCigarettes
                            var newitem = item
                            let time = Date().toString("yyyy:MM:dd:HH:mm:ss")
                            newitem.append(time)
                            AllCigarettes.append(newitem)
                            let todaySmokedCigarettesModel: TodaySmokedCigarettesModel = .init(allCigarettes: AllCigarettes)
                            if let newTodaySmokedTobacoData = todaySmokedCigarettesModel.encode() {
                                todaySmokedTobacoData = newTodaySmokedTobacoData
                            }
                        } else {
                            let todaySmokedCigarettesModel: TodaySmokedCigarettesModel = .init(allCigarettes: [item])
                            if let newTodaySmokedTobacoData = todaySmokedCigarettesModel.encode() {
                                todaySmokedTobacoData = newTodaySmokedTobacoData
                            }
                        }
                    }, label: {
                        selectBotton().overlay {
                            Text("选择").foregroundColor(.black)
                        }
                    })
                }.vAlign(.top)
                    .padding(.leading, 5)
            }
        }
        .hAlign(.leading)
        .padding(.horizontal, 10)
    }

    func loadData() {
        // Simulate loading more data
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            tobacoListModel += TobacoListModel()[stopIndex + 1 ... stopIndex + 10]
            stopIndex += 10
        }
    }
}

struct TobacoInfo_Previews: PreviewProvider {
    struct TobacoInfoWrapper: View {
        @State var searchTobacoText = ""
        @State var userSlectedTobaco = ""
        @State var todayTobacoCount = ""
        @State var todaySmokeConsumption = ""
        @State var lastSmokeTime = ""
        @State var lastSmokeTimeProgess = 0.5
        @State var smokingTimeInterval = Date().halfHour
        @State var isShowingSheet = true
        @State var userData: Data = .init()
        @State private var tobacoListModel: [[String]] = Array(TobacoListModel().prefix(10))
        @State var todaySmokedTobacoData: Data = .init()
        var body: some View {
            TobacoInfo(
                isShowingSheet: $isShowingSheet, searchTobacoText: $searchTobacoText, userSlectedTobaco: $userSlectedTobaco, todayTobacoCount: $todayTobacoCount, todaySmokeConsumption: $todaySmokeConsumption, lastSmokeTime: $lastSmokeTime, lastSmokeTimeProgess: $lastSmokeTimeProgess, smokingTimeInterval: $smokingTimeInterval,
                userData: $userData,
                tobacoListModel: $tobacoListModel, todaySmokedTobacoData: $todaySmokedTobacoData
            )
        }
    }

    static var previews: some View {
        TobacoInfoWrapper()
    }
}
