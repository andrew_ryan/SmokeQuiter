//
//  QuitSmokePlan.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/8.
//

import SwiftUI

struct QuitSmokePlan: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.colorScheme) var colorScheme
    @Binding var smokingTimeInterval: Date
    var body: some View {
        GeometryReader { _ in
            VStack {
                main.navigationTitle("戒烟计划")
            }
        }
    }

    var main: some View {
        // main
        VStack {
            // MARK: set smoke time gap

            HStack {
                TitleView("抽烟时间间隔", colorScheme == .dark ? .white : .black)
                Spacer()
            }

            HStack(spacing: 12) {
                Spacer().frame(width: 10)
                Image(systemName: "clock").font(.title).foregroundColor(colorScheme == .dark ? .blue : .blue)
                Text(smokingTimeInterval.toString("HH:mm")).foregroundColor(.blue)
                Spacer()
            }
            .background(alignment: .center) {
                RoundedRectangle(cornerRadius: 10)
                    .frame(height: 50)
                    .foregroundColor(colorScheme == .dark ? .secondary.opacity(0.1) : .secondary.opacity(0.1))
                    .shadow(color: .black, radius: 5, x: 2, y: 2)
            }.overlay(alignment: .leading) {
                DatePicker("", selection: $smokingTimeInterval, displayedComponents: [.hourAndMinute])
                    .blendMode(.destinationOver)
                    .datePickerStyle(.automatic)
                    .labelsHidden()
                    .environment(\.locale, Locale(identifier: "en_GB")) // Set the locale to a 24-hour format
            }
            .padding(.vertical, 10)

        }.vAlign(.topLeading).hAlign(.leading).padding(.horizontal, 20)
    }

    var header: some View {
        // header
        HStack {
            // back button
            Button(action: {
                dismiss()
            }, label: {
                Image(systemName: "chevron.left")
                    .font(.headline)
                    .bold()
                    .foregroundColor(colorScheme == .dark ? .white : .black)
                    .contentShape(Rectangle())
            })

            // title
            Text("戒烟计划")
                .font(.title2)
                .bold()
                .foregroundColor(colorScheme == .dark ? .white : .black)
                .hAlign(.center)

        }.padding()
    }

    // MARK: titile view for QuitSmokePlan

    @ViewBuilder
    func TitleView(_ value: String, _ color: Color = .black.opacity(0.7)) -> some View {
        Text(value)
            .font(.headline)
            .fontWeight(.bold)
            .foregroundColor(color)
    }
}

struct QuitSmokePlan_Previews: PreviewProvider {
    struct QuitSmokePlanView: View {
        @State var smokingTimeInterval: Date = .init().halfHour
        var body: some View {
            QuitSmokePlan(smokingTimeInterval: $smokingTimeInterval)
        }
    }

    static var previews: some View {
        QuitSmokePlanView()
    }
}
