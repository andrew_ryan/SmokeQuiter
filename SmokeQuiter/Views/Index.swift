//
//  Index.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/8.
//

import SwiftUI

struct Index: View {
    @State var showLeftModal: Bool = false
    @State var leftModalWidth: CGFloat = .zero
    @State var showQuitSmokePlanView: Bool = false
    @State var showMilestoneView: Bool = false
    @State var showTodaySmokedCigarettesList: Bool = false
    @State var showHealthTip: Bool = false
    @State var smokingTimeInterval: Date = .init().threeHour
    @State var stackPath = NavigationPath()
    @AppStorage("todaySmokedTobacoData") var todaySmokedTobacoData: Data = .init()
    @AppStorage("userData") var userData: Data = .init()
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        NavigationStack(path: $stackPath) {
            GeometryReader { geometryReader in
                ZStack {
                    Home(leftModalWidth: $leftModalWidth, showLeftModal: $showLeftModal, smokingTimeInterval: $smokingTimeInterval, userData: $userData, todaySmokedTobacoData: $todaySmokedTobacoData)
                    leftModal(geometryReader: geometryReader)
                }

                .gesture(
                    DragGesture()
                        .onChanged { value in
                            let offset = value.translation.width
                            if offset < 0, leftModalWidth > 0, -offset < geometryReader.size.width * (2 / 4) {
                                leftModalWidth += offset
                                showLeftModal = true
                            }
                        }
                        .onEnded { _ in
                            withAnimation(.linear) {
                                if leftModalWidth < 150 {
                                    leftModalWidth = .zero
                                    showLeftModal = false
                                } else {
                                    leftModalWidth = geometryReader.size.width * (2 / 3)
                                    showLeftModal = true
                                }
                            }
                        }
                )
            }
        }
    }

    @ViewBuilder
    func leftModal(geometryReader _: GeometryProxy) -> some View {
        if showLeftModal {
            Color.black.opacity(0.3)
                .edgesIgnoringSafeArea(.all)
                .onTapGesture {
                    dismissLeftModal()
                }
            HStack {
                VStack {
                    VStack(alignment: .leading, spacing: 0) {
                        dismissButton

                        // 戒烟计划
                        NavigationLink(destination: QuitSmokePlan(smokingTimeInterval: $smokingTimeInterval)) {
                            HStack {
                                Image("plan").resizable().frame(width: 30, height: 30)
                                Text("戒烟计划").foregroundColor(colorScheme == .dark ? .white : .black)
                            }.padding()
                        }
                        // milestone
                        NavigationLink(destination: Milestone(userData: $userData)) {
                            HStack {
                                Image("milestone").resizable().frame(width: 30, height: 30)
                                Text("里程碑").foregroundColor(colorScheme == .dark ? .white : .black)
                            }.padding()
                        }

                        // cigarette
                        NavigationLink(destination: TodaySmokedCigarettes(todaySmokedTobacoData: $todaySmokedTobacoData)) {
                            HStack {
                                Image("cigarette").resizable().frame(width: 30, height: 30)
                                Text("今日抽的烟").foregroundColor(colorScheme == .dark ? .white : .black)
                            }.padding()
                        }

                        // HealthTip
                        NavigationLink(destination: HealthTip()) { HStack {
                            Image("HealthTip").resizable().frame(width: 30, height: 30)
                            Text("健康提示").foregroundColor(colorScheme == .dark ? .white : .black)
                        }.padding()
                        }
                    }
                    .padding(.top, 30)
                    .frame(width: leftModalWidth > 0 ? leftModalWidth : 0)
                    .vAlign(.topLeading)
                    .background(colorScheme == .dark ? .black : .white)
                    .cornerRadius(20)
                }
                .edgesIgnoringSafeArea(.all)

                Spacer()
            }
        }
    }

    var dismissButton: some View {
        Button(action: {
            dismissLeftModal()
        }) {
            Image(systemName: "x.circle")
                .resizable()
                .font(.headline)
                .foregroundColor(.red)
                .frame(width: 30, height: 30)

        }.hAlign(.leading).padding()
    }

    func dismissLeftModal() {
        showLeftModal = false
    }
}

struct Index_Previews: PreviewProvider {
    static var previews: some View {
        Index()
    }
}
