//
//  ContentView.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/10.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView(content: {
            Index()
//            NavigationLink(destination: Text("Destination")) { /*@START_MENU_TOKEN@*/Text("Navigate")/*@END_MENU_TOKEN@*/ }
        })
    }
}

#Preview {
    ContentView()
}
