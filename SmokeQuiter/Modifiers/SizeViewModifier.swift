//
//  SizeViewModifier.swift
//  HealthHabitCompanion
//
//  Created by Ryan Andrew on 2023/6/29.
//

import SwiftUI

/// This modifier wraps a view into a `GeometryReader` and tracks the available space.
///
///     @State var size: CGSize = .zero
///
///     myView.modifier(SizeViewModifier(size: $size))
///
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct SizeViewModifier: ViewModifier {
    @Binding private(set) var size: CGSize

    /// Create a size view modifier from a CGSize binding
    ///
    ///     myView.modifier(SizeViewModifier(size: $size))
    ///
    /// - Parameters:
    ///   - size: CGSize binding
    /// - Returns: A new modifier
    public init(size: Binding<CGSize>) {
        _size = size
    }

    public func body(content: Content) -> some View {
        GeometryReader { proxy in
            content
                .frame(size: proxy.size)
                .onReload(perform: {
                    size = proxy.size
                })
        }
    }
}
