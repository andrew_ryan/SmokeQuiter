//
//  MotivationalStatemenModel.swift
//  HealthHabitCompanion
//
//  Created by Ryan Andrew on 2023/6/24.
//

import Foundation

func MotivationalStatementList() -> [String] {
    ["当你想放弃时，想一想当初为什么开始",
     "摒弃烟雾，获得自由",
     "戒烟是对自己的最好投资，迈向更健康的未来",
     "每一口烟都在破坏你的身体，每一口烟的放弃都是对自己的救赎",
     "戒烟的路上没有捷径，但每一天都是胜利的里程碑",
     "告别烟草，迎接生命的焕新",
     "停止挣扎，选择呼吸自由的空气",
     "你可以掌握你的力量，克服戒烟的挑战",
     "不要因烟而浪费生命，让戒烟成为你追求梦想的开始",
     "为了自己的健康和幸福，向诱惑说不",
     "戒烟是照顾自己、爱护家人的最佳方式",
     "每一天都是新的开始，让戒烟成为自我超越的契机",
     "不再被烟花困扰，你就能品味到真正的自由",
     "无论困难多大，决心坚定就能战胜烟草的诱惑",
     "相信自己，你是戒烟的勇士，胜利属于你",
     "坚持不懈，戒烟的成功将伴随你一生",
     "戒烟不仅改变你的身体，更能改变你的人生",
     "不再让烟雾控制你，重新夺回生活的主导权",
     "向烟草说再见，给自己一个健康的未来",
     "每一次的放弃，都是坚守自己的决心",
     "戒烟，是燃起新生活的第一步",
     "坚强的人选择戒烟，因为他们知道，只有放下过去，才能迎接美好的明天"]
}

func GetMotivationalStatement() -> String {
    let statements = MotivationalStatementList()
    let randomIndex = Int.random(in: 0 ..< statements.count)
    return statements[randomIndex]
}
