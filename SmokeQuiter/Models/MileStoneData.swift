//
//  MileStoneData.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/9.
//

import SwiftUI

struct MileStoneData: Identifiable {
    var id: UUID = .init()
    var day: String
    var tobacoCount: Int

    init(id _: UUID, day: String, tobacoCount: Int) {
        id = UUID()
        self.day = day
        self.tobacoCount = tobacoCount
    }
}
