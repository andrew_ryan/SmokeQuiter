//
//  userDataItemModel.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/10.
//

import Foundation

// MARK: userDataItem

struct userDataItem: Encodable, Decodable {
    var time: Date
    var tobacoListIndex: Int

    init(time: Date, tobacoListIndex: Int) {
        self.time = time
        self.tobacoListIndex = tobacoListIndex
    }
}

// MARK: allUserData

struct allUserData: Encodable, Decodable {
    var allUserItems: [userDataItem]
    init(allUserItems: [userDataItem]) {
        self.allUserItems = allUserItems
    }
}

extension allUserData {
    // MARK: encode allUserData to Data

    func encode() -> Data? {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(self) {
            return data
        } else {
            return nil
        }
    }
}

extension Data {
    // MARK: decode Data to allUserData

    func decode() -> allUserData? {
        let decoder = JSONDecoder()
        if let data = try? decoder.decode(allUserData.self, from: self) {
            return data
        } else {
            return nil
        }
    }
}

func removeIfTimeIsWeekAgo(from allUserItems: [userDataItem]) -> [userDataItem] {
    let calendar = Calendar.current
    let oneWeekAgo = calendar.date(byAdding: .weekOfYear, value: -1, to: Date())!

    let filteredItems = allUserItems.filter { item in
        item.time > oneWeekAgo
    }

    return filteredItems
}

func getAllTodayUserData(from allUserItems: [userDataItem]) -> [userDataItem] {
    let currentDate = Date()
    let calendar = Calendar.current

    let todayTimes = allUserItems.filter { item in
        calendar.isDate(item.time, inSameDayAs: currentDate)
    }.map { $0 }

    return todayTimes
}

func splitDataByDate(v: [String]) -> [[String]] {
    var result: [[String]] = []
    var currentDate = ""

    for item in v {
        let date = String(item.prefix(10))

        if date != currentDate {
            result.append([])
            currentDate = date
        }

        if let _ = result.last {
            result[result.count - 1].append(item)
        }
    }

    return result
}

func splitDataByHour(v: [String]) -> [[String]] {
    var result: [[String]] = []
    var currentDate = ""

    for item in v {
        let date_hour = String(item.prefix(13))

        if date_hour != currentDate {
            result.append([])
            currentDate = date_hour
        }

        if let _ = result.last {
            result[result.count - 1].append(item)
        }
    }
    return result
}

func getMileStoneTodayData(from allUserItems: [userDataItem]) -> [MileStoneData] {
    var mileStone: [MileStoneData] = []
    var allLastSmokeTime = [String]()
    for user in allUserItems {
        let lastSmokeTime = user.time.toString("yyyy:MM:dd:HH:mm:ss")
        allLastSmokeTime.append(lastSmokeTime)
    }
    let splitDataByDateResult = splitDataByHour(v: allLastSmokeTime)
    for re in splitDataByDateResult {
        guard let day = re.first else {
            return mileStone
        }
        mileStone.append(.init(id: UUID(), day: day.prefix(13).description.suffix(2).description, tobacoCount: re.count))
    }
    return mileStone
}

func getMileStoneWeekData(from allUserItems: [userDataItem]) -> [MileStoneData] {
    var mileStone: [MileStoneData] = []
    var allLastSmokeTime = [String]()
    for user in allUserItems {
        let lastSmokeTime = user.time.toString("yyyy:MM:dd:HH:mm:ss")
        allLastSmokeTime.append(lastSmokeTime)
    }
    let splitDataByDateResult = splitDataByDate(v: allLastSmokeTime)
    for re in splitDataByDateResult {
        guard let day = re.first else {
            return mileStone
        }
        mileStone.append(.init(id: UUID(), day: day.prefix(10).description.suffix(5).description.replace(from: ":", to: "-"), tobacoCount: re.count))
    }
    return mileStone
}

func getDaysOfContinuousExecutionPlan(from allUserItems: [userDataItem]) -> Int {
    var allLastSmokeTime = [String]()
    for user in allUserItems {
        let lastSmokeTime = user.time.toString("yyyy:MM:dd:HH:mm:ss")
        allLastSmokeTime.append(lastSmokeTime)
    }
    let splitDataByDateResult = splitDataByDate(v: allLastSmokeTime)
    return splitDataByDateResult.count
}
