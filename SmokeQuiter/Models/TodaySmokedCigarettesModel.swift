//
//  TodaySmokedCigarettesModel.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/11.
//

import Foundation

// MARK: allUserData

struct TodaySmokedCigarettesModel: Encodable, Decodable {
    var allCigarettes: [[String]]
    init(allCigarettes: [[String]]) {
        self.allCigarettes = allCigarettes
    }
}

extension TodaySmokedCigarettesModel {
    // MARK: encode allUserData to Data

    func encode() -> Data? {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(self) {
            return data
        } else {
            return nil
        }
    }
}

extension Data {
    // MARK: decode Data to allUserData

    func decode() -> TodaySmokedCigarettesModel? {
        let decoder = JSONDecoder()
        if let data = try? decoder.decode(TodaySmokedCigarettesModel.self, from: self) {
            return data
        } else {
            return nil
        }
    }
}
