//
//  ForDouble.swift
//  dicos
//
//  Created by Ryan Andrew on 2023/1/16.
//

import Foundation
extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
