//
//  ShareToOtherApp.swift
//  SmokeQuiter
//
//  Created by Ryan Andrew on 2023/8/11.
//

import SwiftUI
import WebKit

class ShareController: UIViewController {
    func shareToOtherApp(_ activityItems: [Any]) {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let viewController = windowScene.windows.first?.rootViewController
        else {
            return
        }
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        viewController.present(activityViewController, animated: true, completion: nil)
    }
}
