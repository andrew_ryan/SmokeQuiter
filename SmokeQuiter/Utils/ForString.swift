//
//  ForString.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/8.
//

#if canImport(Foundation)
    import Foundation
#endif

#if canImport(UIKit)
    import UIKit
#endif

#if canImport(AppKit)
    import AppKit
#endif

#if canImport(CoreGraphics)
    import CoreGraphics
#endif

public extension String {
    func as_bytes() -> [UInt8] {
        Array(utf8)
    }

    func len() -> Int {
        count
    }

    func is_empty() -> Bool {
        count == 0
    }

    func chars() -> [Element] {
        Array(self)
    }

    func split(by: String) -> [String] {
        components(separatedBy: by)
    }

    mutating func push(_ s: String) {
        append(s)
    }

    mutating func clear() {
        self = ""
    }

    mutating func remove(at index: Int) -> Element {
        let removeIndex = self.index(startIndex, offsetBy: index)
        let removed = remove(at: removeIndex)
        return removed
    }

    mutating func mut_replace(from: String, to: String) -> String {
        replacingOccurrences(of: from, with: to)
    }

    func replace(from: String, to: String) -> String {
        replacingOccurrences(of: from, with: to)
    }

    func trim() -> String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func to_uppercase() -> String {
        uppercased()
    }

    func to_lowercase() -> String {
        lowercased()
    }

    func repeat_string(count: Int) -> String {
        String(repeating: self, count: count)
    }

    func rfind(_ s: Character) -> Int? {
        if let index = firstIndex(of: s) {
            let result = distance(from: startIndex, to: index)
            return result
        } else {
            return nil
        }
    }

    func lfind(_ s: Character) -> Int? {
        if let index = lastIndex(of: s) {
            let result = distance(from: startIndex, to: index)
            return result
        } else {
            return nil
        }
    }

    func base64_encode() -> String? {
        let plainData = data(using: .utf8)
        return plainData?.base64EncodedString()
    }

    func base64_decode() -> String? {
        if let data = Data(base64Encoded: self,
                           options: .ignoreUnknownCharacters)
        {
            return String(data: data, encoding: .utf8)
        }
        let remainder = count % 4
        var padding = ""
        if remainder > 0 {
            padding = String(repeating: "=", count: 4 - remainder)
        }
        guard let data = Data(base64Encoded: self + padding,
                              options: .ignoreUnknownCharacters) else { return nil }
        return String(data: data, encoding: .utf8)
    }

    func to_url() -> URL? {
        URL(string: self)
    }

    func urlDecoded() -> String {
        removingPercentEncoding ?? self
    }

    func urlEncoded() -> String {
        addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    func lines() -> [String] {
        var result = [String]()
        enumerateLines { line, _ in
            result.append(line)
        }
        return result
    }

    func contains(_ string: String, caseSensitive: Bool = true) -> Bool {
        if !caseSensitive {
            return range(of: string, options: .caseInsensitive) != nil
        }
        return range(of: string) != nil
    }

    func words() -> [String] {
        let characterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
        let comps = components(separatedBy: characterSet)
        return comps.filter { !$0.isEmpty }
    }

    /// SwifterSwift: Safely subscript string with index.
    ///
    ///        "Hello World!"[safe: 3] -> "l"
    ///        "Hello World!"[safe: 20] -> nil
    ///
    /// - Parameter index: index.
    subscript(safe index: Int) -> Character? {
        guard index >= 0, index < count else { return nil }
        return self[self.index(startIndex, offsetBy: index)]
    }

    #if os(iOS) || os(macOS)
        /// SwifterSwift: Copy string to global pasteboard.
        ///
        ///        "SomeText".copyToPasteboard() // copies "SomeText" to pasteboard
        ///
        func copyToPasteboard() {
            #if os(iOS)
                UIPasteboard.general.string = self
            #elseif os(macOS)
                NSPasteboard.general.clearContents()
                NSPasteboard.general.setString(self, forType: .string)
            #endif
        }
    #endif

    #if os(iOS) || os(tvOS)
        /// SwifterSwift: Check if the given string spelled correctly.
        var isSpelledCorrectly: Bool {
            let checker = UITextChecker()
            let range = NSRange(startIndex ..< endIndex, in: self)

            let misspelledRange = checker.rangeOfMisspelledWord(
                in: self,
                range: range,
                startingAt: 0,
                wrap: false,
                language: Locale.preferredLanguages.first ?? "en"
            )
            return misspelledRange.location == NSNotFound
        }
    #endif
}
