//
//  ForImage.swift
//  HealthHabitCompanion
//
//  Created by Ryan Andrew on 2023/6/29.
//
import SwiftUI
import UIKit

#if canImport(UIKit)
    @available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
    public extension Image {
        /// Create a image with default one
        ///
        ///    let image = Image("photo", defaultImage: "empty-photo")
        ///
        /// - Parameters:
        ///   - name: Image name
        ///   - defaultImage: Default image name
        /// - Returns: A new image
        @inlinable init(_ name: String, defaultImage: String) {
            if let img = UIImage(named: name) {
                self.init(uiImage: img)
            } else {
                self.init(defaultImage)
            }
        }

        /// Create a image with default one
        ///
        ///    let image = Image("photo", defaultSystemImage: "bandage.fill")
        ///
        /// - Parameters:
        ///   - name: Image name
        ///   - defaultSystemImage: Default  system image name
        /// - Returns: A new image
        @available(OSX 10.15, *)
        @inlinable init(_ name: String, defaultSystemImage: String) {
            if let img = UIImage(named: name) {
                self.init(uiImage: img)
            } else {
                self.init(systemName: defaultSystemImage)
            }
        }
    }
#endif

#if canImport(AppKit)
    @available(OSX 10.15, *)
    public extension Image {
        /// Create a image with default one
        ///
        ///    let image = Image("photo", defaultImage: "empty-photo")
        ///
        /// - Parameters:
        ///   - name: Image name
        ///   - defaultImage: Default image name
        /// - Returns: A new image
        @inlinable init(_ name: String, defaultImage: String) {
            if let img = NSImage(named: name) {
                self.init(nsImage: img)
            } else {
                self.init(defaultImage)
            }
        }
    }
#endif

func imageFromText(text: String, font: UIFont, imageSize: CGSize) -> UIImage? {
    let label = UILabel(frame: CGRect(origin: .zero, size: imageSize))
    label.textAlignment = .natural
    label.numberOfLines = 0
    label.font = font
    label.text = text
    label.backgroundColor = UIColor(named: "logo3")
//    label.sizeToFit()

    UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    label.layer.render(in: context)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return image
}
