//
//  LineMarkChart.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/9.
//

import Charts
import SwiftUI

struct PointMarkChart: View {
    var getMileStoneData: [MileStoneData]
    var body: some View {
        Chart {
            ForEach(getMileStoneData, id: \.id) { mileStone in
                BarMark(
                    x: .value("日期", mileStone.day),
                    y: .value("数量", mileStone.tobacoCount)
                )
                .clipShape(RoundedRectangle(cornerRadius: 16))
                .annotation {
                    Text("\(mileStone.tobacoCount)").font(.caption)
                }
            }
        }.frame(height: 300)
    }
}
