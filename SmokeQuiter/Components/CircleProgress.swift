//
//  CircleProgress.swift
//  Smokefree
//
//  Created by Ryan Andrew on 2023/7/30.
//

import SwiftUI

struct CircleProgress: View {
    var progress: Double = 0.5

    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .stroke(Color.gray.opacity(0.2), lineWidth: 4)
                Circle()
                    .trim(from: 0, to: CGFloat(progress))
                    .stroke(Color(hex: "F4E1E1")!, lineWidth: 4)
                    .rotationEffect(.degrees(-90))
            }
            .frame(width: 25, height: 25)
        }
        .padding()
    }
}

struct CircleProgress_Previews: PreviewProvider {
    static var previews: some View {
        CircleProgress()
    }
}
