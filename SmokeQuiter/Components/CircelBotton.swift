//
//  CircelBotton.swift
//  Smokefree
//
//  Created by Ryan Andrew on 2023/7/30.
//

import SwiftUI

struct CircleBotton: View {
    var body: some View {
        ZStack {
            Circle()
                .fill(Gradient(colors: [Color.red, Color.blue]))
                .frame(width: 200, height: 200)
            Text("想抽烟").bold().font(.largeTitle)
        }
    }
}

struct CircelBotton_Previews: PreviewProvider {
    static var previews: some View {
        CircleBotton()
    }
}
