//
//  Ubuntu.swift
//  SmokeFreeApp
//
//  Created by Ryan Andrew on 2023/8/8.
//

import SwiftUI

// MARK: custom font extension

enum Ubuntu {
    case light
    case bold
    case medium
    case regular

    var weight: Font.Weight {
        switch self {
        case .light:
            return .light
        case .bold:
            return .bold
        case .medium:
            return .medium
        case .regular:
            return .regular
        }
    }
}

extension View {
    func Ubuntu(_ size: CGFloat, _ weight: Ubuntu) -> some View {
        font(.custom("ubuntu", size: size))
            .fontWeight(weight.weight)
    }
}
