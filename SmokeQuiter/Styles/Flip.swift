//
//  Flip.swift
//  dicos
//
//  Created by Ryan Andrew on 2023/1/10.
//

import Foundation
import SwiftUI

extension View {
    func flip() -> some View {
        rotationEffect(.radians(.pi))
            .scaleEffect(x: -1, y: 1, anchor: .center)
    }
}
