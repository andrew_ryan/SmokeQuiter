//
//  CustomField.swift
//  dicos
//
//  Created by Ryan Andrew on 2023/1/10.
//

import Foundation
import SwiftUI

struct AddPrefixIcon: ViewModifier {
    var icon: String

    func body(content: Content) -> some View {
        content
            .overlay(
                HStack {
                    Image(systemName: icon)
                        .frame(width: 36, height: 36)
                        .background(.clear)
                        .cornerRadius(14)
                        .offset(x: -46)
                        .foregroundStyle(.secondary)
                        .accessibility(hidden: true)
                    Spacer()
                }
            )
            .foregroundStyle(.primary)
            .padding(15)
            .padding(.leading, 40)
            .background(.thinMaterial)
            .cornerRadius(20)
    }
}

struct AddSuffixButton: ViewModifier {
    var text: String

    func body(content: Content) -> some View {
        content
            .overlay(
                HStack {
                    Button(action: {}, label: {
                        Text(text).frame(width: 48, height: 45)
                            .background(.clear.opacity(0.8))
                            .cornerRadius(14)
                            .foregroundStyle(.primary)
                            .foregroundColor(.white)
                            .accessibility(hidden: false)
                            .padding(.horizontal, 20).padding(.vertical, 30)
                            .offset(x: 200)
                    })
                    Spacer()
                }.hAlign(.trailing)
            )
    }
}

extension View {
    func addPrefixIcon(icon: String) -> some View {
        modifier(AddPrefixIcon(icon: icon))
    }

    func addSuffixButton(text: String) -> some View {
        modifier(AddSuffixButton(text: text))
    }
}
