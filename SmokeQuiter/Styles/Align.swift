//
//  Align.swift
//  dicos
//
//  Created by Ryan Andrew on 2023/1/10.
//

import SwiftUI

// MARK: View extensions

extension View {
    func hAlign(_ alignment: Alignment) -> some View {
        frame(maxWidth: .infinity, alignment: alignment)
    }

    func vAlign(_ alignment: Alignment) -> some View {
        frame(maxHeight: .infinity, alignment: alignment)
    }
}
