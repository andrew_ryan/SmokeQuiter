//
//  WebImg.swift
//  dicos
//
//  Created by Ryan Andrew on 2023/1/15.
//

import SDWebImageSwiftUI
import SwiftUI

struct WebImg: View {
    var imgURL: String = ""

    var body: some View {
        WebImage(url: URL(string: imgURL))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFit()
    }
}

struct WebImg_Previews: PreviewProvider {
    static var previews: some View {
        WebImg(imgURL: "https://gitcode.net/dnrops/get_tobaco_info/-/raw/master/tobaco_images/0.png")
    }
}
